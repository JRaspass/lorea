# lorea

A command line utility to execute commands on filesystem changes.

The interface should be fairly familiar for users of [reflex]. See
[the examples section](#examples) below for some possible ways to use it.

## Installation

    $ zef install App::Lorea

## Usage

    lorea [options] -- command [args]

## Options

        --all                    Include all normally ignored files
    -c, --config     STRING      The path to a configuration file
        --help                   Display usage information
    -R, --ignore     REGEX ...   A regular expression to ignore filenames
    -r, --regex      REGEX ...   A regular expression to match filenames
    -e, --sequential             Run commands sequentially from config file
    -s, --start-service          Run command as a service
        --substitute STRING      Set the string to be used as placeholder
    -v, --verbose                Print tracing information
        --version                Print the version of the command and compiler

Options marked with `...` can be provided multiple times.

Values marked as `REGEX` must be compilable as Raku [regular expressions].

## Overview

When `lorea` is run, it will monitor your current directory, and any
directories under it, for any file-system changes. When a file is changed
or added, it will be matched against the patterns you've specified (see
[below](#matching) for details about how the matching is done).

If the event matches, the command you've specified will be run, or
restarted if you enabled the `--start-service` flag.

If the command you've specified includes the `{}` string, this will be
replaced by the absolute path of the file that has changed. All instances
of this placeholder string will be replaced. If you need to use the `{}`
string for something else, you can specify a different placeholder string
with the `--substitute` option.

### Matching

Patterns to match can be specified to `lorea` with either `--regex` or
`--ignore`. Both of these options take Raku regular expressions, with the
difference that any match for the former will trigger an event, while every
match for the latter will explicitly _not_ trigger one.

Paths are normalised before being matched, and converted to paths relative
to the current directory without a leading `./`. Matching directories will
have a trailing slash (`/`).

If no pattern is specified, all file-system changes will match.

### Default ignores

By default, `lorea` ignores some common patterns to make it easier to use
in most cases.

*   Files and directories with a leading dot (`.`), which are considered
    hidden in Unix-like systems.

*   Common swap files, including those ending in `.swp`, `.swx`, and `~`.

The `--all` option disables these defaults. This flag will take effect if
set either at the top level, or in any of the watchers specified in the
configuration file (see below).

You can see the global ignores by setting the `--verbose` option.

## Configuration

The `--config` option can be used to specify a configuration to use to start
several watchers at the same time. The behaviour of these watchers can be
set independently.

The format of this file is intentionally very limited. A valid file will look
something like this:

    # Rebuild SCSS when it changes
    -r '\.scss$' -- \
       sh -c 'sass {} `basename {} .scss`.css'

    # Restart server when ruby code changes
    -s -r '\.rb$' -- \
        ./bin/run_server.sh

Each line will be interpreted as the parameters to a separate invocation of
`lorea`. Lines must not include the executable name (that will be filled in
internally). The parameters to a single invocation can be broken into multiple
lines if they end with a trailing backslash (`\`).

Empty lines, and lines beginning with a hash (`#`) will be ignored.

With the exception of `--help` , `--sequential`, and `--config` itself, all
options are valid within a configuration file. If the `--config` option is
set, the only other flags that are allowed at the top level are `--verbose`
and `--sequential`.

### Sequential execution

When using a config file to run multiple simultaneous commands, each of them
will be started asynchronously whenever possible. This means that, while there
will only be one instance of a particular command, two different commands may
overlap. This is usually what you want.

As a concrete example, consider this config file:

    -- sh -c 'for i in `seq 1 5`; do sleep 0.1; echo first; done'
    -- sh -c 'for i in `seq 1 5`; do sleep 0.1; echo second; done'

When this runs, you might see something like this:

    second
    first
    second
    first
    first
    second
    second
    first
    second
    first

Note that, since both commands started at the same time, the output of both is
interleaved. If this is not what you want, you can ensure only one command runs
at a time with the `--sequential` flag used at the top-level (it is not allowed
in config files). In this case, the output might instead look like this:

    second
    second
    second
    second
    second
    first
    first
    first
    first
    first

Note in this case that even when using `--sequential` there is no guarantee
that the commands will be executed in the order they appear in the config
file.

## Batching

Part of what `lorea` does is apply some heuristics to batch together file
changes. There are many reasons that files change on disk, and these changes
frequently come in large bursts. For instance, when you save a file in your
editor, it probably makes a tempfile and then copies it over the target,
leading to several different changes. A more dramatic example will happen
when you do a source-control checkout, in which case there is no limit to
the number of files that might change at once.

If `lorea` did not batch these changes, this could lead to your computer
slowing down to a crawl, or the whole process crashing.

There are two modes to batch these changes.

If your command is marked as a service, or has no placeholders in it, then
`lorea` assumes that individual changes are not as important as the fact that
_a change took place_. In this case changes will be collected, and a single
command execution will trigger once a short amount of time has passed
between them.

Otherwise, changes will be collected using the same logic as above, but
_every unique file change_ will trigger a separate execution of your command.

## Examples

Every time a `.raku` or `.rakumod` file changes, print a string with the
name of the modified file:

    lorea --regex '\.raku(mod)?$' -- echo "Change in {}"

or the same thing, but taking advantage of Raku's [regular expressions]:

    lorea --regex '".raku" [mod]? $' -- echo "Change in {}"

[reflex]: https://github.com/cespare/reflex
[regular expressions]: https://docs.raku.org/language/regexes

# See Also

*   [Overwatch](https://raku.land/github:tony-o/Overwatch)

    A similar program with a longer history, this was unknown to me when
    development in App::Lorea started. It has fewer dependencies but supports
    a simpler feature set.
