unit package App::Lorea::Watcher:ver<0.2.6>;

=begin pod

=head2 NAME

App::Lorea::Watcher - General use file recursive file watching subs

=head2 SYNOPSIS

=begin code

use App::Lorea::Watcher;

my $supply = $path.&watch-recursive(
    debounce => 0.5,
    exclude  => rx{ '~' $ }, # Ignore files ending in ~
);

whenever $supply { ... }

=end code

=head2 DESCRIPTION

This module exports a single subroutine to make it easier to recursively
watch a directory for file changes. It is currently bundled in with
App::Lorea, but it does not depend on it for proper use.

=head2 SUBROUTINES

=head3 watch-recursive

=begin code
sub watch-recursive(
    $root,        # The root from where recursion will start
    :$exclude,    # A matcher to exclude
    :$debounce,   # Seconds to wait before emitting the same path
) returns Supply
=end code

Returns a live Supply of IO::Notification::Change with changes to
any file or directory under C<$root> recursively. This includes any
changes to the direct contents of C<$root>. C<$root> can be anything
on which C<.IO> can be called.

An optional C<$debounce> numeric parameter may be provided. If so, the
same path will not be emitted until at least that number of seconds have
passed, or a large enough number of events have been seen. This number
can be set with the C<$cache-size> parameter.

If provided, C<$exclude> is matched against the result of calling
C<App::Lorea::Watcher::normalise> on the path relative to C<$root>. Any
matching paths will I<not> be sent through the Supply.

Based on https://github.com/zoffixznet/perl6-Ticket-Trakr

=end pod

# Copied from relevant bits of Hash::LRU, to avoid dependency
my class Cache is Hash {
    has int $!size is built = 20;
    has str @!keys;

    method AT-KEY ( ::?CLASS:D: \key ) is raw is hidden-from-backtrace {
        self!SEEN-KEY(key) if self.EXISTS-KEY(key);
        nextsame
    }

    method ASSIGN-KEY ( ::?CLASS:D: \key, \value ) is hidden-from-backtrace {
        self!SEEN-KEY(key);
        nextsame
    }

    method BIND-KEY ( ::?CLASS:D: \key, \value ) is hidden-from-backtrace {
        self!SEEN-KEY(key);
        nextsame
    }

    method STORE ( \to_store ) is hidden-from-backtrace {
        callsame;
        self!INIT-KEYS;
        self!SEEN-KEY($_) for self.keys;
        self
    }

    method !INIT-KEYS ( --> Nil ) { @!keys = () }

    method !SEEN-KEY ( Str(Any) $key --> Nil ) {
        if @!keys.elems -> int $elems {
            my int $i = -1;
            Nil while ++$i < $elems && @!keys.AT-POS($i) ne $key;

            if $i < $elems {
                @!keys.splice($i,1);
            }
            elsif $elems == $!size {
                self.DELETE-KEY(@!keys.pop);
            }
        }

        @!keys.unshift($key);
    }
}

sub watch-recursive(
         $root,             # The root from where recursion will start
        :$exclude,          # A matcher to exclude
        :$debounce,         # Seconds to wait before emitting the same path
    Int :$cache-size = 100, # Maximum number of items to cache for debouncing
    --> Supply
) is export {
    my $cache = Cache.new( size => $cache-size );
    my $supplier = Supplier.new;

    start react {
        my sub watch-it($p) {
            CATCH {
                default {
                    say "ERROR: { .^name }";
                }
            }

            whenever IO::Notification.watch-path($p) {
                my $path = .path;

                if .event ~~ FileRenamed && $path.IO.d {
                    .&watch-it for find-dirs $path, :$exclude;
                }

                next if .event !~~ FileChanged;
                next unless $path.IO.e;

                next if $exclude
                    && $path.&normalise($root) ~~ $exclude;

                if $debounce {
                    next if $cache{$path}
                        && now - $cache{$path} < $debounce;

                    $cache{$path} = now;
                }

                $supplier.emit: $_;
            }
        }

        .&watch-it for find-dirs ~$root, :$exclude;
    }

    $supplier.Supply;
}

# Normalise a path for matching. Paths will be relative to C<$root>
# (or to C<$*CWD> if none is given). Directories are guaranteed to have
# a trailing '/'. Normalised paths do not have a leading './'.
our sub normalise ( $path, $root = $*CWD ) is export(:normalise) {
    with $path.IO {
        my $name = .relative($root);
        $name ~= '/' if .d && !.ends-with('/');
        return $name;
    }
}

# Lazily returns a list of directories found recursively under the directory
# specified as C<$root>. C<$root> can be anything on which C<.IO> can be called.
#
# If provided, C<$exclude> is matched against the result of calling
# C<App::Lorea::Watcher::normalise> on the path relative to C<$root>.
my sub find-dirs (
    $root,     # The root from where recursion will start
    :$exclude, # A matcher to exclude directories
 ) {
    my %seen;
    my @stack = $root.IO;
    gather while @stack {
        with @stack.pop -> $path {
            CATCH {
                when X::IO::Dir { note "Could not read $path, skipping..." }
                default {
                    say "UNHANDLED ERROR: { .^name }";
                }
            }

            next unless $path.d;
            next if %seen{$path}++;
            next if $exclude && $path.&normalise($root) ~~ $exclude;

            @stack.append: $path.dir;
            $path.take;
        }
    }
}

=begin pod

=head2 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head2 COPYRIGHT AND LICENSE

Copyright 2020 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it
under the Artistic License 2.0.

=end pod
